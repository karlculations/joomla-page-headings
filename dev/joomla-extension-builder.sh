#!/bin/bash

echo

# Get current directory.
CDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Get the full path to the script (we may need to call it again, but may have already switched directories).
SCRIPT="`readlink -f $0`"

function showinfo {
	echo -e "INFO: $1"
}

function showerror {
	echo -e "ERROR: $1"
}


# Make sure the required folders exist.

# Source folder.
if [ $# -gt 0 -a -d "$CDIR/$1" ]; then
	cd "$CDIR/$1"
	SRCDIR="`pwd`"
elif [ $# -gt 0 -a -d "$1" ]; then
	cd "$1"
	SRCDIR="`pwd`"
else
	cd "$CDIR/../source" 2> /dev/null
	if [ $? -ne 0 ]; then
		showerror 'Unable to find source directory.'
		exit 3
	fi
	SRCDIR="`pwd`"
fi


# Temp folder.
TMPDIR="/tmp/joomla-builder/`basename $SRCDIR`"
rm -rf "$TMPDIR"
mkdir -p "$TMPDIR"
if [ ! -d "$TMPDIR" ]; then
	showerror 'Unable to create tmp directory.'
	exit 1
fi
if [ ! -w "$TMPDIR" ]; then
	showerror 'Unable to write to tmp.'
	exit 2
fi


# Output folder.
cd "$CDIR/bin" 2> /dev/null
if [ $? -ne 0 ]; then
	cd ~ 2> /dev/null
	if [ $? -ne 0 ]; then
		showerror 'Unable to find bin folder or home folder.'
		exit 4
	fi
fi
BINDIR="`pwd`"


# Find XML file and use it to determine extension details.

XMLFILE=`ls -1 "$SRCDIR"/*.xml | head -1 2> /dev/null`
if [ $? -ne 0 ]; then
	showerror "Unable to find XML file in '$SRCDIR'"
	exit 5
fi

EXTVER="`cat "$XMLFILE" | awk '/<version>/ { gsub("</?version>","",$0); print $1 }' | tr -d '[[:space:]]'`"

EXTTYPE="`cat "$XMLFILE" | grep -Eo 'extension.* type="[^"]+' | sed 's/.*"//'`"
case $EXTTYPE in
	component)
		EXTNAME=`basename "$XMLFILE" '.xml'`
		if [ -z "`echo $EXTNAME | grep 'com_'`" ]; then
			EXTNAME="com_$EXTNAME"
		fi
		EXTTYPE="Component"
		;;
	module)
		EXTNAME=`basename "$XMLFILE" '.xml'`
		if [ -z "`echo $EXTNAME | grep 'mod_'`" ]; then
			EXTNAME="mod_$EXTNAME"
		fi
		EXTTYPE="Module"
		;;
	package)
		EXTNAME=`cat "$XMLFILE" | awk '/<packagename>/ { gsub("</?packagename>","",$0); print $1 }' | tr '[:upper:]' '[:lower:]' | tr -d [:space:]`
		EXTTYPE="Package"
		;;
	plugin)
		EXTNAME=`basename "$XMLFILE" '.xml'`
		if [ -z "`echo $EXTNAME | grep 'plg_'`" ]; then
			EXTNAME="plg_$EXTNAME"
		fi
		EXTTYPE="Plugin"
		;;
	template)
		EXTNAME=`cat "$XMLFILE" | awk '/<name>/ { gsub("</?name>","",$0); print $1 }' | tr '[:upper:]' '[:lower:]' | tr -d [:space:]`
		EXTTYPE="Template"
		;;
	*)
		showerror 'Unable to determine extension type (the XML file must define the type) or type is not supported.'
		exit 6
		;;
esac


# Determine output file name.
OUTPUT="$BINDIR/$EXTNAME-$EXTVER.zip"


# Check whether a log file has been specified.
# This tracks the individual extensions that need to be added to the package archive.
log=
if [ $# -gt 1 ]; then
	log=$2
fi


printf "%9s %s\n" "Temp:" "$TMPDIR"
printf "%9s %s\n" "Source:" "$SRCDIR"
printf "%9s %s\n" "Output:" "$OUTPUT"
printf "%9s %s\n" "Type:" "$EXTTYPE"
if [ -n "$log" ]; then
	printf "%9s %s\n" "Log:" "$log"
fi

# Check for composer dependencies
if [ -f "$SRCDIR/composer.lock" ]; then
	# File was found
	# If composer.lock exists, run composer install to install the specified dependency versions from the lock file
	printf "%9s %s\n" "Composer:" "Using $SRCDIR/composer.lock"
	cd $SRCDIR && composer install
else
	printf "%9s %s\n" "Composer:" "$SRCDIR/composer.lock not found"
fi

if [ "$EXTTYPE" == "Package" ]; then

	# Get the extensions comprising the package from the package's XML file, build them, and create the package installer.
	
	for PACKAGEPART in `grep '<file ' $XMLFILE | sed -e 's/.*id="//' -e 's/".*//'`; do

		PACKAGEPATH=`readlink -f "$SRCDIR/../$PACKAGEPART"`
		log="$TMPDIR/package.log"

		if [ -d "$PACKAGEPATH" ]; then
			"$SCRIPT" "$PACKAGEPATH" "$log"
			EXITCODE=$?
			if [ $EXITCODE -ne 0 ]; then
				showerror "Unable to build part of package: $PACKAGEPART.\nGot exit code $EXITCODE"
				exit 8
			fi
		else
			showerror "Unable to find $PACKAGEPATH\n'$PACKAGEPART' was referenced in $XMLFILE\nExiting..."
			exit 7
		fi

	done

	# Copy files/folders to temp folder.

	cp -a "$SRCDIR"/* "$TMPDIR"
	rm -rf `find "$TMPDIR" -name .svn`


	# Add extensions (without the version number to make it easier to manange the package's XML file).
	# Then remove log file so it's not added to the archive.
	mkdir -p "$TMPDIR/packages"
	for EXT in `cat $log`; do
		SIMPLENAME=`basename "$EXT" | sed -r 's/-[0-9]+(\.[0-9]+)+\.zip/.zip/'`
		cp -a "$EXT" "$TMPDIR/packages/$SIMPLENAME"
	done
	rm -rf "$log"


	# Compress.

	echo
	printf "* Compressing $OUTPUT "
	rm -f "$OUTPUT"
	cd "$TMPDIR" && zip -r "$OUTPUT" * > /dev/null
	echo "Done."

	echo
	showinfo 'Build completed successfully :)'
	echo

else

	# An individual extension was specified (not a package) so build it.


	# Copy files/folders to temp folder.

	cp -a "$SRCDIR"/* "$TMPDIR"
	rm -rf `find "$TMPDIR" -name .svn`


	# Compress.

	echo
	printf "* Compressing $OUTPUT "
	rm -f "$OUTPUT"
	cd "$TMPDIR" && zip -r "$OUTPUT" * > /dev/null
	echo "Done."


	# Update log (if there is one).
	
	if [ -n "$log" ]; then
		echo "$OUTPUT" >> "$log"
	fi


	# Show a success message only if not in package mode (in which case the package-building code must show the message).
	if [ -z "$log" ]; then
		echo
		showinfo 'Build completed successfully.'
		echo
	fi
fi


exit 0
