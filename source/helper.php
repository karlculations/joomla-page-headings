<?php
/**
 * Menu Item Title module helper file
 * 
 * @package    Joomla
 * @subpackage Modules
 */

defined('_JEXEC') or die('Restricted access');

class ModMenuItemTitle {
	static function getTitle() {
		//Get active menu item title
		$active = JFactory::getApplication()->getMenu()->getActive();
		$title = $active->title;


		if ($title == "Blog" || $title == "") {
			$input = JFactory::getApplication()->input;
			$id = $input->getInt('id'); //get the article ID
			// Get Content Instance
        	$article = JTable::getInstance('content');
        	$article->load($id);
			$title = $article->get('title') == "Blog 6"? "Blog" : $article->get('title'); // Set the article title if the title isn't Blog 6. For whatever reason BPG has this as the article title.
		}

		// return the results
		return $title;
	}
}
