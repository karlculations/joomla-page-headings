<?php
/**
 * Menu Item Title Module
 * 
 * @package    Joomla
 * @subpackage Modules
 */

// no direct access
defined('_JEXEC') or die;

// include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$title = ModMenuItemTitle::getTitle();

require JModuleHelper::getLayoutPath('mod_menu_item_title');
