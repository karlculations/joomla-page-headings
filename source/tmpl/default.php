<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 
?>

<?= '<div id="menu-item-title" class="' . $params->get("additionalClasses") . '">'; ?>

<?php if($params->get("headerType") == "h1") { ?>
    <h1>
        <?= $title; ?>
    </h1>
<?php } else if ($params->get("headerType") == "h2") { ?>
    <h2>
        <?= $title; ?>
    </h2>
<?php } else if ($params->get("headerType") == "h3") { ?>
    <h3>
        <?= $title; ?>
    </h3>
<?php } else if ($params->get("headerType") == "h4") { ?>
    <h4>
        <?= $title; ?>
    </h4>
<?php } ?>

</div>
